import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponentComponent } from './lazy-component.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyExampleRoutingModule } from './lazy-example.routing.module';

@NgModule({
  declarations: [LazyComponentComponent],
  imports: [
    LazyExampleRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LazyExampleModule { }
