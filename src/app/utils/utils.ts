import * as jwt_decode from 'jwt-decode';
import { FormGroup, FormArray, AbstractControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';

export function decodeToken(token: string): any {
    try {
        return jwt_decode(token);
    } catch (Error) {
        return null;
    }
}

export function getFormGroup(form: FormGroup, name): FormGroup {
    return form.get(name) as FormGroup;
}

export function getFormArray(form: FormGroup, name): FormArray {
    return form.get(name) as FormArray;
}

export function getFormFieldsValidity(form: FormGroup, getValues = false) {
    return Observable.create((observer) => {
        form.valueChanges.subscribe((value) => {
            const object = mapFormControls(form, getValues);
            observer.next(object);
        });
    })
}

function mapFormControls(form: FormGroup, getValues: boolean) {
    let object = {};
    const controls: any = form.controls;
    for (let control in controls) {
        if (controls[control]['controls']) {
            object[control] = mapFormControls(controls[control], getValues);
        } else {
            if (getValues) object[control] = { value: controls[control].value, valid: controls[control].valid }
            else object[control] = controls[control].valid;
        }
    }
    return object;
}

export function updateValidity(form: FormGroup, controls) {
    if (!Array.isArray(controls)) return;
    controls.forEach(c => {
        form.get(c).updateValueAndValidity();
    })
}

export function requiredIf(object, propname, value = null): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let isValid = { 'forbiddenValue': { value: control.value } };
        if (value == null || value == undefined) {//Se usassi !value e value fosse il numero 0 la condizione sarebbe true
            if (object && (object[propname] != null || object[propname] != undefined || object[propname] != "") && control.value != "") isValid = null;
        } else {
            if (object && object[propname] == value && control.value != "") isValid = null;
            else if (object && object[propname] != value) isValid = null;
        }
        return isValid;
    };
}

export function requiredIfNot(object, propname, value = null) {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let isValid = { 'forbiddenValue': { value: control.value } };
        if (value == null || value == undefined) {
            if (object && (object[propname] || object[propname] == "") && control.value != "") {
                isValid = null;
            } else if (object && object[propname] != "") {
                isValid = null;
            }
        } else {
            if (object && object[propname] != value && control.value != "") isValid = null;
            else if (object && object[propname] == value) isValid = null;
        }
        return isValid;
    };
}

export function requiredIfNotForm(form: FormGroup, controlName) {
    return (control: AbstractControl): { [key: string]: any } | null => {
        let isValid = { 'forbiddenValue': { value: control.value } };
        if (form.get(controlName).value == "" && control.value != "") {
            isValid = null;
        } else if (form.get(controlName).value != "") {
            isValid = null;
        }
        return isValid;
    };
}

export function parseNumberAsDecimal(value, decimals = undefined) {
    let number: any;
    if (value) {
        number = Number(value);
        if (number % 1 == 0) number = parseFloat((Math.round(number.valueOf() * 100) / 100).toString()).toFixed(2);
        else if (countDecimals(number) < 2) number = number.valueOf().toFixed(2);
        else if (decimals && countDecimals(number) > decimals) number = number.valueOf().toFixed(decimals);
        else if (!decimals && countDecimals(number) > 6) number = number.valueOf().toFixed(6);
        number = number.valueOf();
    }
    return number;
}

export function countDecimals(number: Number) {
    if (Math.floor(number.valueOf()) === number.valueOf()) return 0;
    return number.toString().split(".")[1].length || 0;
}

export function removeEmptyProperties(object) {
    for (let key in object) {
        if (Array.isArray(object[key])) {
            if (object[key].length === 0) {
                delete object[key];
            } else {
                const array = object[key];
                for (let i = 0; i < array.length; i++) {
                    if (typeof array[i] == 'object') {
                        array[i] = removeEmptyProperties(array[i]);
                    }
                }
            }
        } else if (typeof object[key] == 'object') {
            object[key] = removeEmptyProperties(object[key]);
            if (Object.keys(object[key]).length === 0) {
                delete object[key];
            }
        } else if (object[key] == '') {
            delete object[key];
        }
    }
    return object;
}

export function deepCopy(jsonObject) {// Va bene solo per strutture dati di dimensioni ridotte e senza prototype
    // if (Array.isArray(jsonObject) && jsonObject.find(el => typeof el == 'object') == null) {
    if (Array.isArray(jsonObject)) {
        // return Object.assign([], jsonObject);
        const newArray = [];
        jsonObject.forEach(o => newArray.push(deepCopy(o)));
        return newArray;
    }
    try {
        return JSON.parse(JSON.stringify(jsonObject));
    } catch (error) {
        console.log(error);
    }
}

export function getBase64Mimetype(base64: string) {
    const mimetypeArr = base64.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
    if (mimetypeArr && mimetypeArr.length) {
        return mimetypeArr[1];
    }
    return "";
}

export function getSafeBase64(base64: string) {
    // return base64.replace(/^data:[a-zA-Z\/]*;base64,/, ""); // Non funziona sempre
    return base64.split(',')[1];
}

export function getSafeBase64Size(base64Safe: string) {
    return atob(base64Safe).length;
}

export function getBase64Size(base64: string) {
    const safeb64 = getSafeBase64(base64);
    return atob(safeb64).length;
}