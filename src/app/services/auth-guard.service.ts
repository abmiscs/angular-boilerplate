import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad } from "@angular/router";
import { Injectable } from "@angular/core";

import { map, take } from 'rxjs/operators';

import * as fromApp from '../store/app.reducers';
import * as fromAuth from '../store/auth/auth.reducers';
import { Store } from "@ngrx/store";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private store: Store<fromApp.AppState>,
        private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.store.select('auth').pipe(
            take(1),
            map((authState: fromAuth.State) => {
                if (!authState.authenticated) this.router.navigate(['/signin']);
                return authState.authenticated;
            })
        )
    }

    canLoad(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.store.select('auth').pipe(
            take(1),
            map((authState: fromAuth.State) => {
                if (!authState.authenticated) this.router.navigate(['/signin']);
                return authState.authenticated;
            })
        )
    }

}