import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => event, (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    const error: HttpErrorResponse = err;
                    // Controllo se l'erroe è dovuto a token
                    return next.handle(request); // Funzionerà?
                }
            }));
    }

}