import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { switchMap, take, map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";

import * as fromApp from '../../store/app.reducers';
import * as fromAuth from '../../store/auth/auth.reducers';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {

    private requests: HttpRequest<any>[] = [];

    // constructor(private store: Store<fromApp.AppState>,
    //     private loadingSvc: LoadingService) { }

    constructor(private store: Store<fromApp.AppState>) { }

    // removeRequest(req: HttpRequest<any>) {
    //     const i = this.requests.indexOf(req);
    //     if (i >= 0) {
    //         this.requests.splice(i, 1);
    //     }
    //     this.loadingSvc.isLoading.next((this.requests.length > 0));
    // }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return this.store.select('auth').pipe(
            take(1),
            switchMap((authState: fromAuth.State) => {
                this.requests.push(req);
                // this.loadingSvc.isLoading.next(true);
                let newReq = req;

                if (authState.access_token != null) {
                    const copiedReq = req.clone({
                        headers: req.headers.append('x-auth', `${authState.access_token}`)
                    });
                    newReq = copiedReq;
                    this.requests.splice(this.requests.indexOf(req), 1);
                    this.requests.push(newReq);
                }

                return next.handle(newReq);

                // return Observable.create(observer => {
                //     const sub = next.handle(newReq).subscribe(
                //         event => {
                //             if (event instanceof HttpResponse) {
                //                 this.removeRequest(newReq);
                //                 observer.next(event);
                //             }
                //         },
                //         err => { this.removeRequest(newReq); observer.error(err); },
                //         () => { this.removeRequest(newReq); observer.complete(); }
                //     );

                //     return () => {
                //         this.removeRequest(newReq);
                //         sub.unsubscribe();
                //     }
                // })
            })
        )
    }
}