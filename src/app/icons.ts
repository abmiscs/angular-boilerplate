import { library } from '@fortawesome/fontawesome-svg-core';

import { faCoffee, faUsers, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { faUserCircle, faPlusSquare } from '@fortawesome/free-regular-svg-icons';

export function icons() {
    library.add(faCoffee);
    library.add(faUserCircle);
    library.add(faUsers);
    library.add(faSignInAlt);
    library.add(faPlusSquare);
}