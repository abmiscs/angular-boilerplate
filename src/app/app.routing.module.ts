import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleComponentComponent } from './components/example-component/example-component.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'example' },
    { path: 'example', pathMatch: 'full', component: ExampleComponentComponent },
    { path: 'lazy', loadChildren: './lazy-component/lazy-example.module#LazyExampleModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }