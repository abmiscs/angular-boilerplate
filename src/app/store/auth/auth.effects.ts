import { AppConfig } from '../../config/AppConfig';
import { Router } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects'
import { map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { defer, of } from 'rxjs'
import { decodeToken } from 'src/app/utils/utils';

import * as fromApp from '../app.reducers';
import * as fromAuth from './auth.reducers';
import * as AuthActions from './auth.actions';

import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private store$: Store<fromApp.AppState>,
        private http: HttpClient,
        private router: Router) { }

    // It gets executed as soon as the application loads
    @Effect()
    init$ = defer(async () => {
        try {
            const auth = JSON.parse(localStorage.getItem('auth'));
            if (!auth) {
                return {
                    type: AuthActions.LOGOUT
                }
            }
            const decodedToken = decodeToken(auth.access_token);
            if ((Date.now() / 1000 | 0) >= decodedToken.exp) {
                if (auth.refresh_token) {
                    const decodedRT = decodeToken(auth.refresh_token);
                    if ((Date.now() / 1000 | 0) >= decodedRT.exp) {
                        // also refresh token expired, user must login again
                        return {
                            type: AuthActions.LOGOUT
                        }
                    } else {
                        return {
                            type: AuthActions.RT_FROM_LOGIN,
                            payload: { refresh_token: auth.refresh_token }
                        }
                    }
                } else {
                    return {
                        type: AuthActions.LOGOUT
                    }
                }
            } else {
                const payload = {
                    access_token: auth.access_token,
                    refresh_token: auth.refresh_token,
                    identityId: auth.identityId,
                    identityEmail: auth.identityEmail
                }
                return {
                    type: AuthActions.LOGIN,
                    payload
                }

            }
        } catch (e) {
            console.log('[AuthEffects#defer] error', e);
            return {
                type: AuthActions.LOGOUT
            }
        }
    });

    @Effect()
    authLogin = this.actions$.pipe(
        ofType(AuthActions.TRY_LOGIN),
        map((action: AuthActions.TryLogin) => {
            return action.payload;
        }),
        switchMap((authData: { email: string, password: string }) => {
            const body = new HttpParams()
                .set('username', authData.email)
                .set('password', authData.password)
                .set('grant_type', 'password')
                .set('client_id', 'sample')
                .set('client_secret', 'sample');

            return this.http.post('rootApiUrl' + '/oidc/token', body.toString(), {
                headers: new HttpHeaders({
                    'content-type': 'application/x-www-form-urlencoded',
                    'showLoading': 'true'
                })
            }).pipe(
                map((res: any) => {
                    const decodedToken = decodeToken(res.access_token);
                    const payload = {
                        access_token: res.access_token,
                        refresh_token: res.refresh_token,
                        identityId: decodedToken.sub,
                        identityEmail: authData.email
                    }
                    localStorage.setItem('auth', JSON.stringify(payload));
                    return {
                        type: AuthActions.LOGIN,
                        payload
                    }
                }),
                catchError((error) => {
                    return of({
                        type: AuthActions.AUTH_ERROR,
                        payload: error
                    })
                })
            );
        }),

    )

    @Effect()
    rtFromState = this.actions$.pipe(
        ofType(AuthActions.RT_FROM_STATE),
        withLatestFrom(this.store$.select('auth')),
        map(([action, authState]) => {
            return {
                action,
                authState
            }
        }),
        switchMap((data: { action: AuthActions.RtFromState, authState: fromAuth.State }) => {
            const payload = data.action.payload;
            const authState = data.authState;
            const body = new HttpParams()
                .set('refresh_token', payload.refresh_token)
                .set('grant_type', 'refresh_token')
                .set('client_id', 'sample')
                .set('client_secret', 'sample');

            return this.http.post('rootApiUrl' + '/oidc/token', body.toString(), {
                headers: new HttpHeaders({
                    'content-type': 'application/x-www-form-urlencoded',
                    'showLoading': 'true'
                })
            }).pipe(
                map((res: any) => {
                    const decodedToken = decodeToken(res.access_token);
                    const decodedIdToken = decodeToken(res.id_token);
                    const payload = {
                        access_token: res.access_token,
                        refresh_token: res.refresh_token,
                        identityId: decodedToken.sub,
                        identityEmail: decodedIdToken.email
                    }
                    localStorage.setItem('auth', JSON.stringify(payload));
                    return {
                        type: AuthActions.LOGIN, // QUESTO NON DEVE FARE IL LOGIN
                        payload
                    }
                }),
                catchError((error) => {
                    return of({
                        type: AuthActions.AUTH_ERROR,
                        payload: error
                    })
                })
            )
        })
    )

    @Effect()
    rtFromLogin = this.actions$.pipe(
        ofType(AuthActions.RT_FROM_LOGIN),
        map((action: any) => {
            return action.payload;
        }),
        switchMap((payload: { refresh_token: string }) => {
            const body = new HttpParams()
                .set('refresh_token', payload.refresh_token)
                .set('grant_type', 'refresh_token')
                .set('client_id', 'sample')
                .set('client_secret', 'sample');

            return this.http.post('rootApiUrl' + '/oidc/token', body.toString(), {
                headers: new HttpHeaders({
                    'content-type': 'application/x-www-form-urlencoded',
                    'showLoading': 'true'
                })
            }).pipe(
                map((res: any) => {
                    const decodedToken = decodeToken(res.access_token);
                    const decodedIdToken = decodeToken(res.id_token);
                    const payload = {
                        access_token: res.access_token,
                        refresh_token: res.refresh_token,
                        identityId: decodedToken.sub,
                        identityEmail: decodedIdToken.email
                    }
                    localStorage.setItem('auth', JSON.stringify(payload));
                    return {
                        type: AuthActions.LOGIN,
                        payload
                    }
                }),
                catchError((error) => {
                    return of({
                        type: AuthActions.AUTH_ERROR,
                        payload: error
                    })
                })
            )
        })
    )

    @Effect()
    tryLogout = this.actions$.pipe(
        ofType(AuthActions.TRY_LOGOUT),
        map(() => {
            localStorage.removeItem('auth');
            this.router.navigate(['']);
            return {
                type: AuthActions.LOGOUT
            }
        })
    )

    @Effect()
    signup = this.actions$.pipe(
        ofType(AuthActions.TRY_SIGNUP),
        map((action: AuthActions.TrySignup) => {
            return action.payload;
        }),
        switchMap((signupData: { email: string, password: string, identityType: string }) => {
            const body = {
                email: signupData.email,
                password: signupData.password,
                identityType: signupData.identityType
            }
            return this.http.post('rootApiUrl' + '/signup', body, { headers: new HttpHeaders({ 'showLoading': 'true', 'content-type': 'application/json' }) }).pipe(
                map((response) => {
                    return {
                        type: AuthActions.TRY_LOGIN,
                        payload: body
                    }
                }),
                catchError((error) => {
                    return of({
                        type: AuthActions.AUTH_ERROR,
                        payload: error
                    })
                })
            )
        })
    )

    @Effect({ dispatch: false })
    authError = this.actions$.pipe(
        ofType(AuthActions.AUTH_ERROR),
        map((action: any) => {
            console.log(action.payload);
        })
    )

}