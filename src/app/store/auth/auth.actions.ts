import { Action } from '@ngrx/store';

export const TRY_LOGIN = 'TRY_LOGIN';
export const LOGIN = 'LOGIN';
export const RT_FROM_LOGIN = 'RT_FROM_LOGIN'; // usato in faso di login, vuole l'RT come parametro
export const RT_FROM_STATE = 'RT_FROM_STATE'; // usato durante un'altra chiamata per rinnovare il token
export const TRY_LOGOUT = 'TRY_LOGOUT';
export const LOGOUT = 'LOGOUT';
export const SET_TOKEN = 'SET_TOKEN';
export const TRY_SIGNUP = 'TRY_SIGNUP';
export const AUTH_ERROR = 'AUTH_ERROR';

export class TryLogin implements Action {
    readonly type = TRY_LOGIN;
    constructor(public payload: { email: string, password: string }) { }
}

export class Login implements Action {
    readonly type = LOGIN;

    constructor(public payload: any) { }
}

export class RtFromLogin implements Action {
    readonly type = RT_FROM_LOGIN;
    constructor(public payload: { refresh_token: string }) { }
}

export class TryLogout implements Action {
    readonly type = TRY_LOGOUT;
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class SetToken implements Action {
    readonly type = SET_TOKEN;

    constructor(public payload: string) { }
}

export class TrySignup implements Action {
    readonly type = TRY_SIGNUP;

    constructor(public payload: { email: string, password: string, identityType: string }) { }
}

export class RtFromState implements Action {
    readonly type = RT_FROM_STATE;

    constructor(public payload: any) { }
}

export class AuthError implements Action {
    readonly type = AUTH_ERROR;
    constructor(public payload) { }
}

export type AuthActions = TryLogin | Login | TryLogout | Logout | TrySignup | AuthError | RtFromLogin | RtFromState;