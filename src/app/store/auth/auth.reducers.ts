import * as AuthActions from './auth.actions';

export interface State {
    authenticated: boolean,
    access_token: string,
    refresh_token: string,
    identityId: string,
    identityEmail: string
}

const initialState: State = {
    authenticated: false,
    access_token: null,
    refresh_token: null,
    identityId: null,
    identityEmail: null
}

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
    switch (action.type) {
        case AuthActions.LOGIN:
            return {
                ...state,
                authenticated: true,
                access_token: action.payload.access_token,
                refresh_token: action.payload.refresh_token,
                identityId: action.payload.identityId,
                identityEmail: action.payload.identityEmail
            }
        case AuthActions.LOGOUT:
            return {
                ...state,
                ...initialState
            }
        default:
            return state;
    }
}