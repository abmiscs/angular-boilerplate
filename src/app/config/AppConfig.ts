import { environment } from './../../environments/environment';

export class AppConfig {

    static api = environment.apiBaseURL;

    static services = {
        baseService: AppConfig.api + "/service/endpoint"
    }

}