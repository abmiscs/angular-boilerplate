export const environment = {
  production: false,
  envName: 'staging',
  apiBaseURL: 'http://localhost:8080/'
};
