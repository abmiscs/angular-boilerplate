export const environment = {
  production: true,
  envName: 'prod',
  apiBaseURL: 'http://localhost:8080/'
};
